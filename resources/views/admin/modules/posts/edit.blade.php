@extends('admin.layouts.master')
@section('styles')
    <style>
        label.error {
            display: inline-block;
            color: #d71212;
            width: 100%;
            font-size: 13px;
            font-weight: 600;
            text-transform: capitalize;
            margin-top: 5px;
        }
        #custom_slug {
            margin-top: 10px;
        }
        #text-custom-slug {
            color: #333;
            font-weight: 600;
            font-size: 14px;
            margin-left: 10px;
        }
        .thumbnail{
            width: 163px;
            height: 163px;
            margin: 10px;
            float: left;
        }
        #clear{
            display:none;
            float: right;
            margin-bottom: 5px;
        }
        #result {
            display: none;
            width: 100%;
        }
        .upload-gallery {
            border: 4px dotted #cccccc;
            min-height: 200px;
            width: 100%;
            margin-top: 0;
        }
        .custom-tab nav {
            background-color: rgba(43, 34, 34, 0.03);
            border: 1px solid rgba(0,0,0,.125);
        }
        .nav-tabs {
            border-bottom: none !important;
        }
        .nav-tabs .nav-link {
            border: none !important;
            border-top-left-radius: 0 !important;
            border-top-right-radius: 0 !important;
        }
        .nav-tabs .nav-item {
            margin-bottom: 0 !important;
            color: #333;
            font-weight: 600;
        }
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            background-color: rgb(247, 247, 247);
        }
        .image-append {
            border: 1px solid #f8f8f8;
            background-color: #272c33;
        }
        .upload-gallery {
            background-image: url(http://localhost:8000/a-images/noimg.jpg);
            background-color: #f3f3f3;
            width: 100%;
            background-size: contain;
        }
        #clear {
            display: none;
            width: 100%;
            text-align: right;
            background: transparent;
            border: transparent;
            color: #333;
            font-weight: 600;
            text-decoration: underline;
            float: none !important;
            margin-bottom: 0 !important;
            cursor: pointer;
        }
        #nav-tabContent {
            height: 570px;
            overflow-y: scroll;
            padding-right: 1rem !important;
            padding-left: 0 !important;
        }
        .custom-tab-right {
            height: 300px;
            overflow-y: scroll;
            padding-right: 0.5rem !important;
        }
        .post-module .card-header {
            padding: 8px 10px !important;
        }
        .post-module .card-body {
            padding: 10px !important;
        }
        .post-module .card-body.select-custom-height {
            height: 205px;
            overflow-y: scroll;
        }
        .checkbox {
            padding: 0 20px 15px 20px;
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            align-items: center;
        }
        .checkbox-input {
            margin-right: 10px;
        }

        .input-item {
            width: 50%;
        }
    </style>
@endsection
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Sửa bài viết</h4>
                        </div>
                        <div class="card-body">
                            <form id="form-edit-post" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="post_edit_id" id="post_id" value="{{ $posts->id }}">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="custom-tab">
                                            <nav>
                                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                                    <a class="nav-item nav-link active show" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">Bài viết</a>
                                                    <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">Thông tin bài viết</a>
                                                    <a class="nav-item nav-link" id="custom-nav-contact-tab" data-toggle="tab" href="#custom-nav-contact" role="tab" aria-controls="custom-nav-contact" aria-selected="false">Thông số bài viết</a>
                                                </div>
                                            </nav>
                                            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                                <div class="tab-pane fade active show" id="custom-nav-home" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input"
                                                                                         class=" form-control-label">Tên bài viết</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="post_title" name="post_title"
                                                                   placeholder="Tên bài viết" class="form-control" value="{!! old('post_title', $posts->post_title) !!}">
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input"
                                                                                         class=" form-control-label">Slug tiêu đề</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" name="post_slug" id="post_slug" disabled
                                                                   value="{{ old('post_slug', $posts->post_slug) }}"
                                                                   class="form-control">
                                                            <input type="checkbox" id="custom_slug"><span id="text-custom-slug">Custom Slug</span>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="textarea-input" class=" form-control-label">Nội dung bài viết</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <textarea name="post_content" id="post_content" rows="5" placeholder="Nội dung bài viết" class="form-control" value="{!! old('post_content', $posts->post_content) !!}">{{ $posts->post_content }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="textarea-input" class=" form-control-label">Mô tả ngắn</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <textarea name="post_excerpt" id="post_excerpt" rows="5" placeholder="Mô tả ngắn" class="form-control">{{ $posts->post_excerpt }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="select" class=" form-control-label">Trạng
                                                                thái</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="post_status" id="post_status" class="form-control">
                                                                <option value="1">Hiện</option>
                                                                <option value="2">Ẩn</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                                                    <p>Comming soon</p>
                                                </div>
                                                <div class="tab-pane fade" id="custom-nav-contact" role="tabpanel" aria-labelledby="custom-nav-contact-tab">
                                                    <p>Comming soon</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="post-avatar">
                                            <div class="form-group">
                                                <label for="">Ảnh đại diện</label>
                                                <div class="upload-image">
                                                    <div class="input-file">
                                                        <input type='file' onchange="readURLAvatar(this);" name="post_avatar"
                                                               id="post_avatar" style="display: none"/>
                                                    </div>
                                                    <div class="image-append">
                                                        <img style="width: 100%; height: 260px;" id="avatar" onclick="chooseFile()" src="{{ asset('uploads/posts/images/'.$posts->post_thumbnail) }}" alt="your image"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="custom-tab-right">
                                            <div class="post-module">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Hỗ trợ Seo</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row form-group">
                                                            <div class="col-12 col-md-12"><label for="textarea-input"
                                                                                                 class=" form-control-label">Seo Description</label>
                                                            </div>
                                                            <div class="col-12 col-md-12"><textarea name="seo_description"
                                                                                                    id="seo_description"
                                                                                                    rows="3" placeholder="Seo Description..."
                                                                                                    class="form-control">{{ $posts->seo_description }}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-12 col-md-12"><label for="textarea-input"
                                                                                                 class=" form-control-label">Seo Keyword</label>
                                                            </div>
                                                            <div class="col-12 col-md-12"><textarea name="seo_keyword"
                                                                                                    id="seo_keyword"
                                                                                                    rows="3" placeholder="Seo Keyword..."
                                                                                                    class="form-control">{{ $posts->seo_keyword }}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="card-footer" style="width: 100%">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Save
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div>
@endsection
@section('ajax')
    <script>
        $('#form-edit-post').validate({
            rules: {
                post_title: {
                    required: true,
                },
            },
            messages: {
                post_title: {
                    required: 'Vui lòng nhập tên bài viết',
                }
            },
            submitHandler: function (data) {
                var post_title = $("#post_title").val();
                var post_slug = $("#post_slug").val();
                var post_content = $("#post_content").val();
                var post_excerpt = $("#post_excerpt").val();
                var post_avatar = $('#post_avatar').prop('files')[0];
                var post_status = $("#post_status").val();
                var seo_description = $("#seo_description").val();
                var seo_keyword = $("#seo_keyword").val();
                var post_id = $("#post_id").val();
                var data_src_image = $("#avatar").prop('src');

                form_data = new FormData();
                form_data.append('post_title',post_title);
                form_data.append('post_slug',post_slug);
                form_data.append('post_content',post_content);
                form_data.append('post_excerpt',post_excerpt);
                form_data.append('post_avatar',post_avatar);
                form_data.append('post_status',post_status);
                form_data.append('seo_description',seo_description);
                form_data.append('seo_keyword',seo_keyword);
                form_data.append('post_id',post_id);
                form_data.append('data_src_image',data_src_image);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('post-update-posts') }}',
                    data: form_data,
                    dataType: 'json',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status == true) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Sửa bài viết thành công',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            window.location.href = "{{ route('page-posts') }}";
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Sửa bài viết thất bại',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            return false;
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    </script>
@endsection
@section('scripts')
    <script>
        $('#custom_slug').on('change', function () {
            if ($(this).prop('checked') == true){
                $('#post_slug').prop('disabled',false);
            }else{
                $('#post_slug').prop('disabled',true);
            }
        });
        function chooseFile() {
            $('#post_avatar').click();
        }
        //file reader custom
        var fileTypes = ['jpg', 'jpeg', 'png'];  //acceptable file types

        function readURLAvatar(input) {
            if (input.files && input.files[0]) {
                var extension = input.files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
                    isSuccess = fileTypes.indexOf(extension) > -1;  //is extension in acceptable types
                if (isSuccess) { //yes
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#avatar').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
        }
    </script>
@endsection
