@extends('admin.layouts.master')
@section('styles')
    <style>
        label.error {
            display: inline-block;
            color: #d71212;
            width: 100%;
            font-size: 13px;
            font-weight: 600;
            text-transform: capitalize;
            margin-top: 5px;
        }
        .input-file{
            padding-left: 20px;
            padding-bottom: 20px;
        }
    </style>
@endsection
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Upload file PDF</h4>
                        </div>
                        <div class="card-body">
                            <form id="form-upload" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="input-file">
                                        <input type='file' name="pdf_file"
                                               id="pdf_file"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="card-footer" style="width: 100%">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Upload
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div>
@endsection
@section('ajax')
    <script>
        $('#form-upload').validate({
            rules: {
                pdf_file: {
                    required: true,
                },
            },
            messages: {
                pdf_file: {
                    required: 'Vui lòng nhập file cần upload',
                }
            },
            submitHandler: function (data) {
                var pdf_file = $('#pdf_file').prop('files')[0];
                form_data = new FormData();
                form_data.append('pdf_file',pdf_file);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('post-uploadfile') }}',
                    data: form_data,
                    dataType: 'json',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status == true) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Upload thành công',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            window.location.href = "{{ route('page-posts') }}";
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Upload thất bại',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            return false;
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    </script>
@endsection
