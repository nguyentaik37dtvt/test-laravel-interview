@extends('admin.layouts.master')
@section('styles')
    <style>
        label.error {
            display: inline-block;
            color: #d71212;
            width: 100%;
            font-size: 13px;
            font-weight: 600;
            text-transform: capitalize;
            margin-top: 5px;
        }
        .input-file{
            padding-left: 20px;
            padding-bottom: 20px;
        }
    </style>
@endsection
@section('main')
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Send Email</h4>
                        </div>
                        <div class="card-body">
                            <form id="form-email" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="text-input"
                                                                     class=" form-control-label">Địa chỉ email</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="email" id="email_add" name="email_add"
                                               placeholder="Địa chỉ email" class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3"><label for="text-input"
                                                                     class=" form-control-label">Tiêu đề email</label></div>
                                    <div class="col-12 col-md-9">
                                        <input type="text" id="email_title" name="email_title"
                                               placeholder="Tiêu đề email" class="form-control">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="textarea-input" class=" form-control-label">Nội dung Email</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <textarea name="email_content" id="email_content" rows="5" placeholder="Nội dung Email" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="card-footer" style="width: 100%">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Send
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div>
@endsection
@section('ajax')
    <script>
        $('#form-email').validate({
            rules: {
                email_add: {
                    required: true,
                },
                email_title: {
                    required: true,
                },
                email_content: {
                    required: true,
                },
            },
            messages: {
                email_add: {
                    required: 'Vui lòng nhập địa chỉ Email',
                },
                email_title: {
                    required: 'Vui lòng nhập tiêu đề của email',
                },
                email_content: {
                    required: 'Vui lòng nhập nội dung của email',
                }
            },
            submitHandler: function (data) {
                var email_add = $('#email_add').val();
                var email_title = $('#email_title').val();
                var email_content = $('#email_content').val();

                form_data = new FormData();
                form_data.append('email_add',email_add);
                form_data.append('email_title',email_title);
                form_data.append('email_content',email_content);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('post-sendemail') }}',
                    data: form_data,
                    dataType: 'json',
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.status == true) {
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Gửi email thành công',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            window.location.href = "{{ route('page-posts') }}";
                        } else {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'Gửi email thất bại',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            return false;
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    </script>
@endsection
