<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Routing Admin */
Route::group(['namespace' => 'Admin'], function () {
    Route::group(['middleware' => 'check-login'], function () {
        Route::get('/s-login', 'AuthController@login')->name('page-login');
        Route::post('/s-login', 'AuthController@postLogin')->name('post-login');
    });
    Route::group(['prefix' => 's-admin', 'middleware' => 'check-logout'], function () {
        Route::get('/', 'AdminController@index')->name('admin-page');
        Route::get('/logout', 'AuthController@logout')->name('post-logout');

        //posts
        Route::get('/posts', 'PostsController@posts')->name('page-posts');
        Route::post('/posts/updateStatus', 'PostsController@updateStatus')->name('update-status-post');
        Route::get('/posts/create', 'PostsController@create')->name('page-create-posts');
        Route::post('/posts/store', 'PostsController@store')->name('post-store-posts');
        Route::get('/posts/edit/{id}', 'PostsController@edit')->name('page-edit-posts');
        Route::post('/posts/update', 'PostsController@update')->name('post-update-posts');
        Route::delete('/posts/delete/{id}', 'PostsController@destroy')->name('post-destroy-posts');
        Route::delete('/posts/delete_multi', 'PostsController@destroy_multi')->name('post-destroy-multi_posts');
        // upload file
        Route::get('/posts/upload', 'PostsController@upload')->name('post-upload-file');
        Route::post('/posts/uploadfile', 'PostsController@uploadfile')->name('post-uploadfile');
        // Send Email
        Route::get('/posts/send-mail', 'PostsController@send_email')->name('post-send-email');
        Route::post('/posts/sendmail', 'PostsController@sendemail')->name('post-sendemail');


    });
});
/* End */
