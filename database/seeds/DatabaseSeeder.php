<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'admin1',
                'email' => 'lxc150896@gmail.com',
                'role_id' => '1',
                'password' => bcrypt('12345'),
            ],
            [
                'name' => 'admin2',
                'email' => 'lxc@gmail.com',
                'role_id' => '2',
                'password' => bcrypt('12345'),
            ],
            [
                'name' => 'admin3',
                'email' => 'admin@gmail.com',
                'role_id' => '3',
                'password' => bcrypt('12345'),
            ],
            [
                'name' => 'admin4',
                'email' => 'nguyentai@gmail.com',
                'role_id' => '1',
                'password' => bcrypt('12345'),
            ],
        ];
        DB::table('admins')->insert($data);
    }
}
