<?php
namespace App\Services\Contracts;
interface PostServiceContract
{
    public function get($columns=['*']);
    public function paginate($option);
    public function store(array $data);
    public function find($id);
    public function update($id, array $data);
    public function destroy($id);
}
