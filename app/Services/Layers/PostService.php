<?php
namespace App\Services\Layers;
use App\Repositories\Contracts\PostRepositoryContract;
use App\Services\Contracts\PostServiceContract;

class PostService implements PostServiceContract
{
    protected $repository;
    public function __construct(PostRepositoryContract $repository)
    {
        return $this->repository = $repository;
    }
    public function get($columns=['*'])
    {
        return $this->repository->get();
    }
    public function find($id)
    {
        return $this->repository->find($id);
    }
    public function paginate($option)
    {
        return $this->repository->paginate($option);
    }
    public function update($id, array $data)
    {
        return $this->repository->update($id, $data);
    }
    public function store(array $data)
    {
        return $this->repository->store($data);
    }
    public function destroy($id)
    {
        return $this->repository->destroy($id);
    }
}
?>
