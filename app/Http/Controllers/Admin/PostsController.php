<?php

namespace App\Http\Controllers\Admin;
use App\Models\Posts;
use App\Traits\UploadTrait;
use App\Services\Contracts\PostServiceContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mail;
use AddAttactment;
use Illuminate\Support\Facades\File;

class PostsController extends Controller
{
    use UploadTrait;
    protected $postService;
    public function __construct(PostServiceContract $postService)
    {
        $this->postService = $postService;
    }

    public function posts()
    {
        $posts = $this->postService->get();
        return view('admin.modules.posts.index',compact('posts'));
    }
    public function create()
    {
        return view('admin.modules.posts.create');
    }
    public function store(Request $request)
    {
        try{
            if ($request->hasFile('post_avatar')) {
                $filename = $request->file('post_avatar')->getClientOriginalName();
                $name = time().$filename;
                $request->file('post_avatar')->move('uploads/posts/images/',$name);
            } else {
                $name = null;
            }
            $posts = $this->postService->store([
                'post_title' => $request->post_title,
                'post_slug' => !empty($request->post_slug) ? $request->post_slug : Str::slug($request->post_title, '-'),
                'post_content' => $request->post_content,
                'post_excerpt' => $request->post_excerpt,
                'post_thumbnail' => $name,
                'post_status' => $request->post_status,
                'seo_description' => $request->seo_description,
                'seo_keyword' => $request->seo_keyword,
            ]);
            return response()->json([
                'status' => true,
            ]);
        }catch (Exception $e){
            Log::debug($e);
            return response()->json(false);
        }
    }
    public function edit($id)
    {
        $posts = $this->postService->find($id);
        return view('admin.modules.posts.edit',compact('posts'));
    }
    public function update(Request $request)
    {
        try{
            $file = $this->postService->find($request->post_id)->post_thumbnail;
            $image_path = "uploads/posts/images/".$file;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            if ($request->hasFile('post_avatar')) {
                $filename = $request->file('post_avatar')->getClientOriginalName();
                $name = time().$filename;
                $request->file('post_avatar')->move('uploads/posts/images/',$name);
            } else {
                $name = basename($request->data_src_image);
            }
            $updated = $this->postService->update($request->post_id,[
                'post_title' => $request->post_title,
                'post_slug' => !empty($request->post_slug) ? $request->post_slug : Str::slug($request->post_title, '-'),
                'post_content' => $request->post_content,
                'post_excerpt' => $request->post_excerpt,
                'post_thumbnail' => $name,
                'post_status' => $request->post_status,
                'seo_description' => $request->seo_description,
                'seo_keyword' => $request->seo_keyword,
            ]);
            return response()->json([
                'status' => true,
            ]);
        }catch (Exception $e){
            Log::debug($e);
            return response()->json(false);
        }
    }
    public function destroy($id) {
        try {
            $file = $this->postService->find($id)->post_thumbnail;
            $image_path = "uploads/posts/images/".$file;
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            $destroy = $this->postService->destroy($id);
            // delete images product
            return response()->json($destroy);
        } catch (Exception $e) {
            Log::debug($e);
            return response()->json(false);
        }
    }
    public function destroy_multi(Request $request)
    {
        try{
            $data_id = $request->data_id;
            $myArray = json_decode($data_id);
            foreach ($myArray as $id){
                $file = $this->postService->find($id)->post_thumbnail;
                $image_path = "uploads/posts/images/".$file;
                if(File::exists($image_path)) {
                    File::delete($image_path);
                }
                $query = DB::table('posts')->where('id', '=', $id);
                $query->delete();
            }
            return response()->json(true);
        } catch (Exception $e) {
            Log::debug($e);
            return response()->json(false);
        }
    }
    public function updateStatus(Request $request)
    {
        try{
            $statusposts = $this->postService->find($request->post_id);
            $statusposts->post_status = $request->status;
            $statusposts->save();
            return respone()->json([
                'result' => $statusposts
            ]);
        }catch (Exception $e){
            Log::debug($e);
            return response()->json(false);
        }

    }
    // upload pdf fiile
    public function upload(){
        return view('admin.modules.posts.upload');
    }
    public function uploadfile(Request $request)
    {
        try{
            if ($request->hasFile('pdf_file')) {
                $filename = $request->file('pdf_file')->getClientOriginalName();
                $name = time().$filename;
                $request->file('pdf_file')->move('uploads/posts/pdf/',$name);
            }
            return response()->json([
                'status' => true,
            ]);
        }catch (Exception $e){
            Log::debug($e);
            return response()->json(false);
        }
    }
    // send email
    public function send_email(){
        return view('admin.modules.posts.email');
    }
    public function sendemail(Request $request){
        try{
            $emails = [
                $request->email_add,
            ];
            $email_title = $request->email_title;
            $data= [
                'email_add' => $request->email_add,
                'email_title' => $request->email_title,
                'email_content' => $request->email_content,
            ];
            $fileList = glob('uploads/posts/pdf/*');
            Mail::send('admin.modules.posts.blanks',$data, function($message) use ($emails,$email_title,$fileList)
            {
                $message->from('nguyenxuanhl60@gmail.com','My Email');
                $message->to($emails);
                $message->subject($email_title);
                $message->attach($fileList[0]);
            });
            return response()->json([
                'status' => true,
            ]);
        }catch (Exception $e){
            Log::debug($e);
            return response()->json(false);
        }
    }
}
