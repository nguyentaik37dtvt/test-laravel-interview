<?php
namespace App\Repositories\Eloquents;
use App\Models\Posts;
use App\Repositories\Contracts\PostRepositoryContract;
class PostRepository implements PostRepositoryContract
{
    protected $post;
    public function __construct(Posts $post)
    {
        $this->post = $post;
    }
    public function get($column = ['*'])
    {
        return $this->post->select($column)->get();
    }
    public function paginate($option)
    {
        return $this->post->paginate($option);
    }
    public function update($id, array $data)
    {
        return $this->post->findOrFail($id)->update($data);
    }
    public function find($id)
    {
        return $this->post->findOrFail($id);
    }
    public function store(array $data)
    {
        return $this->post->create($data);
    }
    public function destroy($id)
    {
        return $this->post->findOrFail($id)->delete();
    }

}
?>
