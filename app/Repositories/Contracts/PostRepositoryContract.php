<?php
namespace App\Repositories\Contracts;
interface PostRepositoryContract
{
    public function get($column=['*']);
    public function paginate($option);
    public function find($id);
    public function store(array $data);
    public function update($id, array $data);
    public function destroy($id);

}
