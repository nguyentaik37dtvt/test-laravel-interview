<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'id', 'post_title', 'post_slug', 'post_excerpt','post_content','post_thumbnail','post_status','seo_description','seo_keyword'
    ];
}
